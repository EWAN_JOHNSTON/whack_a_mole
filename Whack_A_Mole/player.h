#pragma once
#include <SFML/Graphics.hpp>//Libary needed for using sprites, graphics and fonts
#include <vector>//Libary for handling Collections of objects
class Player
{
public:
	// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u screenSize);
	void Input();
	void Update(sf::Time frameTime);

	void Reset(sf::Vector2u screenSize);

	// Variables (data members) used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;

};

