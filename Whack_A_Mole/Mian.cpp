#include <SFML/Graphics.hpp>
#include <SFML/Graphics.hpp> //For creating window
#include <SFML/Audio.hpp> //For palyying audio
#include <cstdlib>
#include <string>
#include <vector>
#include "player.h"

int main()
{
    sf::RenderWindow gameWindow;
    gameWindow.create(sf::VideoMode::getDesktopMode(), "Whack a Sloth", sf::Style::Titlebar | sf::Style::Close); //setting dimensions and titling the window


    //Game Setup

    // Player
    // Declare a texture variable called playerTexture
    sf::Texture playerTexture;
    // Load up our texture from a file path
    playerTexture.loadFromFile("WAM_Assets/Assets/Graphics/player.png");
    // Declare a player object
    Player playerObject(playerTexture, gameWindow.getSize());

    //Holes
    sf::Texture holeTexture;
    holeTexture.loadFromFile("WAM_Assets/Assets/Graphics/hole.png");
    std::vector<sf::Sprite> holes(9, sf::Sprite(holeTexture));

    //Music
    sf::Music gameMusic;
    gameMusic.openFromFile("WAM_Assets/Assets/Audio/music.ogg");
    gameMusic.setVolume(25);
    gameMusic.play();
    
    // Load the victory sound effect file into a soundBuffer
    sf::SoundBuffer victorySoundBuffer;
    victorySoundBuffer.loadFromFile("WAM_Assets/Assets/Audio/victory.ogg");
    // Setup a Sound object to play the sound later, and associate it with the SoundBuffer
    sf::Sound victorySound;
    victorySound.setBuffer(victorySoundBuffer);

    //Title Text
    sf::Font gameFont;
    gameFont.loadFromFile("WAM_Assets/Assets/Fonts/batmfa__.ttf");
    sf::Text titleText;
    titleText.setFont(gameFont);
    titleText.setString("Whack a Sloth");
    titleText.setCharacterSize(24);
    titleText.setFillColor(sf::Color::Cyan);
    titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
    titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);
    
    // Author Text
    sf::Text authorText;
    authorText.setFont(gameFont);
    authorText.setString("by Ewan Johnston");
    authorText.setCharacterSize(16);
    authorText.setFillColor(sf::Color::Red);
    authorText.setStyle(sf::Text::Bold | sf::Text::Italic);
    authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);
    
    // Game Over Text
    // Declare a text variable called gameOverText to hold our game over display
    sf::Text gameOverText;
    // Set the font our text should use
    gameOverText.setFont(gameFont);
    // Set the string of text that will be displayed by this text object
    gameOverText.setString("GAME OVER");
    // Set the size of our text, in pixels
    gameOverText.setCharacterSize(72);
    // Set the colour of our text
    gameOverText.setFillColor(sf::Color::Cyan);
    // Set the text style for the text
    gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
    // Position our text in the top center of the screen
    gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

    //Timer Text
    sf::Text timerText;
    timerText.setFont(gameFont);
    timerText.setString("Time Remaining: 0");
    timerText.setCharacterSize(16);
    timerText.setFillColor(sf::Color::White);
    timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);

    sf::Time timeLimit = sf::seconds(60.0f);
    sf::Time timeRemaining = timeLimit;
    sf::Clock gameClock;

    srand(time(NULL));

    //Score Text
    int score = 0;
    sf::Text scoreText;
    scoreText.setFont(gameFont);
    scoreText.setString("Score: 0");
    scoreText.setCharacterSize(16);
    scoreText.setFillColor(sf::Color::White);
    scoreText.setPosition(30, 30);


    bool gameOver = false;
    while (gameWindow.isOpen())
    {
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                gameWindow.close();
        }
        sf::Event gameEvent;
        while (gameWindow.pollEvent(gameEvent))
        {
            //repeats each event waiting to be process
            if (gameEvent.type == sf::Event::Closed)
            {
                gameWindow.close();
            }
        }//end of pollEvent
        
         //INPUT SECTION

        sf::Time frameTime = gameClock.restart();
        timeRemaining = timeRemaining - frameTime;
        //if time runs out
        
        if (timeRemaining.asSeconds() <= 0)
        {
            // Don't let the time go lower than 0
            timeRemaining = sf::seconds(0);
            // Perform these actions only once when the game first ends:
            if (gameOver == false)
            {
                // Set our gameOver to true now so we don't perform these actions again
                gameOver = true;
                // Stop the main music from playing
                gameMusic.stop();
                // Play our victory sound
                victorySound.play();
            }
        }
        
         
        timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

        // Player keybind input
        playerObject.Input();
        //UPDATE SECTION


        // Move the player
        playerObject.Update(frameTime);

        //DRAW SECTION
        gameWindow.clear(sf::Color(153, 51, 0, 255));// Clear the window to a single colour
        gameWindow.draw(titleText);
        gameWindow.draw(authorText);
        gameWindow.draw(timerText);
        gameWindow.draw(scoreText);
        
        if (!gameOver)
        {
            
            for (int i = 0; i < holes.size(); ++i)
            {
                
                int row = 1;
                int col = 1;
                holes[i].setPosition(gameWindow.getSize().x / 4 * row - 50, gameWindow.getSize().y / 4 * col - 50);
                
                if (i == 2 || i == 5)
                {
                    row = 1;
                    col++;
                }
                else { row++; }
                
            }
            for (int i = 0; i < holes.size(); ++i) { gameWindow.draw(holes[i]); }
            gameWindow.draw(playerObject.sprite);
        }
        gameWindow.display();
        // Only draw these items if the game HAS ended:
        if (gameOver)
        {
            gameWindow.draw(gameOverText);
        }
    }

    return 0;
}